# Private Router

## Installation

> Before configuring the router or updating the firmware, it is recommended to be disconnected from the internet.

The firmware installation has been performed on an old router. `OpenWRT 23.05.3` has been installed on this device.

## Initial Configuration

At system startup, the router will always start with a LAN network `192.168.1.0/24` and the domain [openwrt.lan](http://openwrt.lan). It is necessary to connect from a PC via a LAN port to change the network to `10.0.0.0/24`.

Connect to the router via SSH with the following command:

```sh
ssh root@openwrt.lan
```

### Change Hostname

Change the OpenWRT hostname to its alias (`sigma`):

```sh
uci set system.@system[0].hostname='sigma'
uci commit system
/etc/init.d/system reload
# Reboot system
reboot
```

### Disable Unnecessary Services

Go to web interface [here](http://sigma.lan).

Remove wireless network interfaces under `Network` -> `Wireless`.

### Adjust timezone

Go to `System` -> `System` -> `General settings` and adjust the timezone.

### Assign Addresses

#### DHCP

Access the web interface at [this page](sigma.lan). At the moment, you can log in with the user `root` and no password.

To assign a static address to the server for the external network and to change the addresses of the internal network, go to `Network` -> `Interfaces`. You will see the following options:

- LAN
- WAN
- WAN6

To change the IP of the external network, click the `Edit` button of the `WAN` interface and change the values to the following:

- `General Settings` Tab

| Entry          | Value           |
|----------------|-----------------|
| Protocol       | Static address  |
| IPv4 Address   | 192.168.0.200   |
| IPv4 Netmask   | 255.255.255.0   |
| IPv4 Gateway   | 192.168.0.1     |
| IPv4 Broadcast | 192.168.0.255   |

- `Advanced Settings` Tab (`Next DNS`)

| Entry                     | Value        |
|---------------------------|--------------|
| Use Custom DNS Servers (1)| 45.90.28.254 |
| Use Custom DNS Servers (2)| 45.90.30.254 |

Finally, click the `Save` button.

Next, to change the addresses of the internal network, click the `Edit` button of the `LAN` interface and enter the following values:

- `General Settings` Tab

| Entry           | Value          |
|-----------------|----------------|
| Protocol        | Static address |
| IPv4 Address    | 10.0.0.1       |
| IPv4 Netmask    | 255.255.255.0  |
| IPv4 Gateway    |                |
| IPv4 Broadcast  | 10.0.0.255     |

- `DHCP Server` -> `General Setup` Tab

| Entry | Value |
|-------|-------|
| Start | 101   |
| Limit | 100    |

- `DHCP Server` -> `Advanced Settings` Tab

| Entry | Value |
|-------|-------|
| Force | True  |

- `DHCP Server` -> `IPv6 Settings` Tab: All parameters deactivated

Finally, click the `Save` button and then the `Save & Apply` button. Afterward, restart the device and check if you have access to the new address.

> If there is no connection with the device, restart the network service on your operating system and try again.

> After this configuration, you can connect the device to the internet.

### Security Improvements

Connect to the router via SSH with the following command:

```sh
ssh root@sigma.lan
```

#### Change Password

Change the root password with the `passwd` command.

#### Create User for Sudo

OpenWRT by default operates with the root user. It is advisable to create a common user who works with sudo and log in with it into the device:

```sh
# Update repositories and install software
opkg update
opkg install shadow-useradd
# Configure the new user
useradd cgt --home-dir /home/cgt --shell /bin/ash
# Add password
passwd cgt
# Add user folder
mkdir -p /home/cgt
chown cgt:cgt /home/cgt
```

Then enable SSH access by editing the `/etc/passwd` file and entering (or modifying) the following line:

```sh
cgt:x:1000:1000::/home/cgt:/bin/ash
```

Now, grant temporary privileged access with sudo. Install the sudo program first:

```sh
opkg install sudo
```

Then write the `visudo` command and uncomment the specified line below.

```sh
## Uncomment to allow members of group sudo to execute any command
%sudo ALL=(ALL:ALL) ALL
```

Create the `sudo` group:

```sh
opkg install shadow-groupadd
groupadd --system sudo
```

And finally, link our user to the sudo group:

```sh
opkg install shadow-usermod
usermod -a -G sudo cgt
```

With these steps, we can use the new user for SSH connections.

> Although the user works for SSH, they cannot log in through the web interface. Additional steps may be necessary for this, but information was not found on the internet.

To prevent the system from requesting the password each time we use sudo, run the `visudo` command and add the following line:

```sh
# Use sudo without password
cgt ALL=(ALL) NOPASSWD:ALL
```

#### Use Certificate for SSH Connections

##### Root

On the local machine, do the following to generate the key. The file will be named `sigma`:

```sh
ssh-keygen -b 4096 -t rsa -f ~/.ssh/sigma
```

The above command will create two files in the `.ssh` folder.

Go to `System` -> `Administration` -> `SSH-Keys` and add the key saved in the `sigma.pub` file.

Change the certificate permissions:

```sh
sudo chmod 600 ~/.ssh/sigma.pub
```

Try to authenticate again with the following command. It should not ask for a password:

```sh
ssh root@sigma.lan -i ~/.ssh/sigma
```

##### Another User

To connect with a public key with a user other than root (for example, a user with sudo), follow the same steps:

On the local machine:

```sh
# Generate the new key
ssh-keygen -b 4096 -t rsa -f ~/.ssh/sigma_cgt
# Add the key on the router
ssh-copy-id -i ~/.ssh/sigma_cgt.pub cgt@sigma.lan
# Change the permissions of the key on the local machine
sudo chmod 600 ~/.ssh/sigma_cgt.pub
# Gain access to the remote server with the key
ssh cgt@sigma.lan -i .ssh/sigma_cgt
```

> If access keys are lost, you can reactivate password SSH access from the web interface to regenerate them.

#### Change SSH Port

From the OpenWRT web interface, go to `System` -> `Administration` -> `SSH Access` -> `Dropbear instance` and change the following parameters:

| Entry                          | Value |
|--------------------------------|-------|
| Port                           | 2222  |
| Password authentication        | False |
| Allow root logins with password| False |

After clicking `Save & Apply`, connect using the new port:

```sh
ssh root@sigma.lan -p 2222 -i .ssh/sigma
ssh cgt@sigma.lan -p 2222 -i .ssh/sigma_cgt
```

#### Add Banner

Create a custom banner. (Text created with [Text to ASCII Art Generator](https://patorjk.com/software/taag/#p=display&f=Slant&t=Sigma%0A) using the `Slant` font):

```sh
# Connect to ssh with sudo user
ssh cgt@sigma.lan -p 2222 -i .ssh/sigma_cgt
# Install vim
sudo opkg update
sudo opkg install vim
# Add vim patch for non root user
sudo ln -s vimrc /usr/share/vim/defaults.vim
sudo cat /usr/share/vim/vimrc > ~/.vimrc
# Create and edit file
sudo vim /etc/config/dropbear.banner.txt
```

```
                   _____ _
                  / ___/(_)___ _____ ___  ____ _
                  \__ \/ / __ `/ __ `__ \/ __ `/
                 ___/ / / /_/ / / / / / / /_/ /
                /____/_/\__, /_/ /_/ /_/\__,_/
                       /____/
##############################################################
#                      Welcome to Sigma                      #
#         All connections are monitored and recorded         #
#  Disconnect IMMEDIATELY if you are not an authorized user! #
##############################################################
```

Now configure the SSH server to display the banner every time a connection attempt is made. Edit the configuration:

```sh
sudo vim /etc/config/dropbear
```

And add the following line:

```
option BannerFile '/etc/config/dropbear.banner.txt'
```

### Installation of Additional Software

```sh
sudo opkg update
# Install Htop to view running processes
sudo opkg install htop
```

## Advanced Configuration

### Address Reservation

To reserve addresses for the main devices, go to `Network` -> `DHCP and DNS` -> `Static Leases` and add the following entries:

| Hostname  | MAC Address  | IPv4 Address |
|-----------|--------------|--------------|
| sigma     | (Device MAC) | 10.0.0.1     |
| alia      | (Device MAC) | 10.0.0.2     |
| zero      | (Device MAC) | 10.0.0.3     |
| vile      | (Device MAC) | 10.0.0.98    |
| copy-x    | (Device MAC) | 10.0.0.99    |
| mr-x      | (Device MAC) | 10.0.0.100   |

Save and apply the changes. It may be necessary to disconnect and reconnect to the network for the host to get the configured IP.

### Configure Access Point

See [Documentation](./README_AP.md)

### Subdomain Configuration

To configure the subdomains of the network, go to `Network` -> `Hostnames` and add the following entries:

| Hostname           | IP Address        |
|--------------------|-------------------|
| alia.lan           | 10.0.0.3 (alia)   |
| git.zero.lan       | 10.0.0.3 (zero)   |
| portainer.zero.lan | 10.0.0.3 (zero)   |
| mr-x.lan           | 10.0.0.100 (mr-x) |

After finishing, save and apply the changes.

### Ad Blocker

To install the ad blocker, it is necessary to do it via the command line. For this, log in via SSH and enter the following commands:

(Taken from [openwrt.org](https://openwrt.org/docs/guide-user/services/ad-blocking))

```sh
# Install packages
sudo opkg update
sudo opkg install adblock

# Provide web interface
sudo opkg install luci-app-adblock
```

Restart the router. Then in the web interface, you will see a new section `Services` -> `Adblock`.

In this section, activate the following options:

- `Force local DNS`: `true`
- `Forced zones`: [ `lan` ]
- `Forced ports`: [ 53, 853, 5353 ]

With these steps, the ad blocker should work. You can test it by opening the [doubleclick.net](https://doubleclick.net) page. If the browser indicates that the page has not been found, the installation was successful.

> It may be useful to activate other sources to block more sites (for example, for parental control), but make sure the sources are not too large, as they can slow down the device.

### Wake On Lan (WOL)

We will activate Wake On Lan in OpenWRT to be able to turn on the main PC and other devices in the future.

Install the following package to be able to turn on devices from the OpenWRT web interface:

```sh
sudo opkg update
sudo opkg install luci-app-wol
```

Also, there is a Telegram bot task to turn on the main PC via WOL.

### Configuration Backup

> Run a backup after performing the above steps and installing the [Telegram bot](../../../scripts/bots/sigma/README.md)

It is always important to make backups of the router's configuration. For this, go to `System` -> `Backup / Flash firmware` and make a backup every time a significant change is made.

## Traffic Capture (optional)

### Capture of WAN Port Traffic

(Taken from [openwrt.org](https://openwrt.org/docs/guide-user/firewall/misc/tcpdump_wireshark))

Install tcpdump to capture traffic:

```sh
sudo opkg update
sudo opkg install tcpdump
```

You can capture all packets from the WAN interface (eth0.1):

```sh
sudo killall tcpdump; sudo tcpdump -n -i eth0.1
```

You can also capture traffic and save it to a pcap file, to analyze it later with wireshark:

```sh
sudo tcpdump -i eth0.1 -Z cgt -W 5 -C 10 -w $HOME/pcap/wan_$(date +%Y%m%d)_
```

Keep in mind that:
* The WAN port is eth0.1.
* The `Z` parameter is the owner user of the generated files.
* The `W` parameter is the "rotating buffer," which is 5 files.
* The `C` parameter is the file size, multiplied by 1,000,000 Bytes.
* The `w` parameter will be the file name prefix.

In summary, the above command will save a maximum of 5 capture files of approximately 10 MB each.

You can also capture traffic with Wireshark from a local machine with the following command:

```sh
ssh cgt@sigma.lan -p 2222 -i .ssh/sigma_cgt sudo tcpdump -i eth0.1 -U -s0 -w - 'not port 2222' | wireshark -k -i -
```

Keep in mind that:
* The `cgt` user is being used.
* The OpenWRT server is at 10.0.0.1.
* The WAN port is eth0.1.
* The SSH port is 2222.

Similarly, you can use the above command to capture traffic on another machine and review it later with Wireshark.

```sh
ssh cgt@sigma.lan -p 2222 -i .ssh/sigma_cgt sudo tcpdump -i eth0.1 -U -s0 -w - 'not port 2222' | tee > wan_$(date +%Y%m%d).pcap
```

> For this last procedure, SSH access with a certificate must be enabled and not by password.