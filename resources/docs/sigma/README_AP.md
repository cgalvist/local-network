# Access Point

## Installation

> Before configuring the router or updating the firmware, it is recommended to be disconnected from the internet.

> When installing or updating `OpenWRT`, it is advisable to do so by connecting the network cable directly to the PC to avoid problems due to IP changes.

The firmware installation has been performed on a router. `OpenWRT 23.05.3` has been installed on this device.

## Initial Configuration

At system startup, the router will always start with a LAN network `192.168.1.0/24` and the domain [openwrt.lan](http://openwrt.lan). It is necessary to connect from a PC via a LAN port at the beginning of this guide.

We connect to the router via SSH with the following command:

```sh
ssh root@openwrt.lan
```

### Change Hostname

We will change the OpenWRT hostname to its alias (`alia`):

```sh
uci set system.@system[0].hostname='alia'
uci commit system
/etc/init.d/system reload
# Reboot system
reboot
```

### Adjust timezone

Go to `System` -> `System` -> `General settings` and adjust the timezone.

### Settings

Taken from [openwrt.org](https://openwrt.org/docs/guide-user/network/wifi/dumbap)

Go to web interface [here](http://alia.lan).

#### Initial Configuration

In the web interface, go to `Network` -> `Interfaces` and edit the `LAN` interface. Configure the following data:

- `General Settings` Tab

| Entry          | Value          |
|----------------|----------------|
| Protocol       | Static address |
| IPv4 Address   | 10.0.0.2       |
| IPv4 Netmask   | 255.255.255.0  |
| IPv4 Gateway   | 10.0.0.1       |
| IPv4 Broadcast | 10.0.0.255     |

Go back to `Network` -> `Interfaces` and edit the `LAN` interface.

- `Advanced Settings` Tab

| Entry                  | Value    |
|------------------------|----------|
| Use Custom DNS Servers | 10.0.0.1 |

- `DHCP Server` -> `General setup` Tab

| Entry           | Value |
|-----------------|-------|
| Ignore interface| True  |

- `DHCP Server` -> `IPv6 Settings` Tab

| Entry          | Value    |
|----------------|----------|
| RA-Service     | Disabled |
| DHCPv6-Service | Disabled |
| NDP-Proxy      | Disabled |

Save and apply the changes.

Disconnect the access point from the PC and connect it to the main router.

#### Disable Unnecessary Services

Go to `System` -> `Startup` and disable the following services by clicking the `Enabled` button:

- firewall
- dnsmasq
- odhcpd

Then restart the device.

### Security Improvements

Connect to the router via SSH with the following command:

```sh
ssh root@alia.lan
```

#### Change Password

Change the root password with the `passwd` command.

#### Use Certificate for SSH Connections

##### Root

On the local machine, do the following to generate the key. The file will be named `alia`:

```sh
ssh-keygen -b 4096 -t rsa -f ~/.ssh/alia
```

The above command will create two files in the `.ssh` folder.

Go to `System` -> `Administration` -> `SSH-Keys` and add the key saved in the `alia.pub` file.

Change the permissions of the certificate:

```sh
sudo chmod 600 ~/.ssh/alia.pub
```

Try to authenticate again with the following command. It should not ask for a password:

```sh
ssh root@alia.lan -i ~/.ssh/alia
```

#### Change SSH Port

From the OpenWRT web interface, go to `System` -> `Administration` -> `SSH Access` -> `Dropbear instance` and change the following parameters:

| Entry                           | Value |
|---------------------------------|-------|
| Port                            | 2222  |
| Password authentication         | False |
| Allow root logins with password | False |

> Before making these changes, verify that public key authentication is working.

After clicking `Save & Apply`, connect using the same port:

```sh
ssh root@alia.lan -p 2222 -i .ssh/alia
```

#### Add Banner

Create a custom banner. (Text created with [Text to ASCII Art Generator](https://patorjk.com/software/taag/#p=display&f=Slant&t=Alia%0A) using the `Slant` font):

```sh
# Create and edit file
vim /etc/config/dropbear.banner.txt
```

```
                          ___    ___
                         /   |  / (_)___ _
                        / /| | / / / __ `/
                       / ___ |/ / / /_/ /
                      /_/  |_/_/_/\__,_/
#############################################################
#                      Welcome to Alia                      #
#         All connections are monitored and recorded        #
# Disconnect IMMEDIATELY if you are not an authorized user! #
#############################################################
```

Now configure the SSH server to display the banner every time a connection attempt is made. Edit the configuration:

```sh
vim /etc/config/dropbear
```

And add the following line:

```
option BannerFile '/etc/config/dropbear.banner.txt'
```

### Wireless Network Configuration

Go to `Network` -> `Wireless` and make the following adjustments.

#### 2.4 GHz Network

Edit the first `OpenWrt` network:

- `Device configuration` -> `General Setup`

| Entry                          | Value |
|--------------------------------|-------|
| Operating frequency - Mode     | N     |
| Operating frequency - Channel  | auto  |

- `Interface configuration` -> `General Setup`

| Entry    | Value        |
|----------|--------------|
| Mode     | Access Point |
| ESSID    | Alia         |

- `Interface configuration` -> `Wireless Security`

| Entry      | Value                        |
|------------|------------------------------|
| Encryption | WPA2-PSK/WPA3-SAE Mixed Mode |
| key        | <Network Key>                |

Save and apply the changes. Then enable the network and test if authentication works.

#### 5 GHz Network

Edit the second `OpenWrt` network:

- `Device configuration` -> `General Setup`

| Entry                          | Value |
|--------------------------------|-------|
| Operating frequency - Mode     | AC    |
| Operating frequency - Channel  | auto  |

- `Interface configuration` -> `General Setup`

| Entry    | Value        |
|----------|--------------|
| Mode     | Access Point |
| ESSID    | Alia         |

- `Interface configuration` -> `Wireless Security`

| Entry      | Value                        |
|------------|------------------------------|
| Encryption | WPA2-PSK/WPA3-SAE Mixed Mode |
| key        | <Network Key>                |

Save and apply the changes. Then enable the network and test if authentication works.