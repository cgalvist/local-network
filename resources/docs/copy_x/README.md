# Android Device Configuration

## Required Applications

- [F-droid](https://f-droid.org/)
- [Termux](https://f-droid.org/en/packages/com.termux/)
- [Termux:Styling](https://f-droid.org/en/packages/com.termux.styling/)
- [KDE Connect](https://f-droid.org/en/packages/org.kde.kdeconnect_tp/)
- [MiXplorer](https://xdaforums.com/t/app-2-2-mixplorer-v6-x-released-fully-featured-file-manager.1523691/)

## Termux Configuration

> To simplify the process, it is recommended to use the remote keyboard of `KDE Connect` and emulate the `Enter` key with the combinations `CTRL` + `J` or `CTRL` + `M`.

### Configure User

To find out our Termux user, run the command `whoami`. Then, change the password with the following commands:

```sh
# Update repositories and software
apt update && apt -y upgrade
# Install authentication package
apt -y install termux-auth
# Change user password
passwd
```

### Install SSH Server

```sh
# Update OpenSSH and some required packages
apt -y install openssh procps vim
```

After installing OpenSSH, check its configuration with the following command:

```sh
cat $PREFIX/etc/ssh/sshd_config
```

Ensure that the `PasswordAuthentication` option is set to `yes`.

Now run the ssh server daemon with the `sshd` command, and connect via SSH to a remote machine with the following command:

```sh
ssh copy-x.lan -p 8022
```

After these steps and entering the established username and password, you can use Termux remotely.

### Security Enhancements

#### Use Certificate for SSH Connections

On the local machine, do the following to generate the key:

```sh
ssh-keygen -b 4096 -t rsa -f ~/.ssh/copy_x_cgt
```

The above command will create two files in the `.ssh` folder.

Now, to add the key to the server, type the following on the local machine:

```sh
# Copy the certificate to the device
ssh-copy-id -i ~/.ssh/copy_x_cgt.pub -p 8022 cgt@copy-x
# Change the permissions of the public key
sudo chmod 600 ~/.ssh/copy_x_cgt.pub
```

Finally, test the connection to see if it's working:

```sh
ssh cgt@copy-x.lan -p 8022 -i ~/.ssh/copy_x_cgt
```

#### Additional Enhancements

Open the configuration file to change the port and disable password authentication:

```sh
vim $PREFIX/etc/ssh/sshd_config
```

```sh
Port 2222
PasswordAuthentication no
```

Finally, to restart OpenSSH, run the following command **FROM THE APPLICATION**:

```sh
pkill sshd && sshd
```

From the local machine, connect via ssh using the new port:

```sh
ssh cgt@copy-x.lan -p 2222 -i ~/.ssh/copy_x_cgt
```

### Program Installation

General programs:

```sh
apt update && apt -y upgrade
apt -y install git mc tmux htop curl wget rsync iproute2
```

### Shortcut Configuration and Startup

For the `Termux:Widget` plugin, add the following configurations to create shortcuts with custom commands:

```sh
# Create folder
mkdir -p $HOME/.shortcuts/ && cd $HOME/.shortcuts/
# Create script to start ssh server
echo "termux-wake-lock && sshd" > sshd-start.sh
# Create script to stop ssh server
echo "pkill sshd" > sshd-stop.sh
# Add execution permissions to all scripts
chmod +x *.sh
```

Optional: If you want to start Termux on boot with the `Termux:Boot` plugin, add the following script:

```sh
# Create folder
mkdir -p $HOME/.termux/boot/ && cd $HOME/.termux/boot/
# Create script to start ssh server on boot
echo "termux-wake-lock && sshd" > start-sshd.sh
# Add execution permissions to all scripts
chmod +x *.sh
```

### Customization

#### Application Appearance

To change the appearance, long-press the Termux console and select the `Style` menu to change colors or the console font.

Recommended Style:
- Color: `Solarized dark`
- Font: `Ubuntu`

#### Application

To customize the console, edit Termux properties and add the following configuration:

```sh
vim ~/.termux/termux.properties
```

```sh
# Custom setup

# - Enable fullscreen
fullscreen = true
# - Enable UI dark theme
use-black-ui = true
# - Enable bell-character
bell-character = beep
# - Edit additional keys
extra-keys = [ \
 ['ESC','CTRL','ALT','TAB','UP','DOWN','KEYBOARD'] \
]
```

After adding the configuration, apply the changes by running the command `termux-reload-settings` or by closing all open sessions and reopening the application.

### Set Permissions

To enable access to storage, run the command `termux-setup-storage`.

To verify that storage access is working correctly, check that the folder `$HOME/storage` exists and is not empty.

> If the process doesn't work, try it several times or restart the application.

### Install Additional Applications (Optional)

Termux plugins:

```sh
# Termux widget
am start -n org.fdroid.fdroid/org.fdroid.fdroid.views.main.MainActivity -d https://f-droid.org/en/packages/com.termux.widget/
# Termux API (after installing, grant all necessary permissions)
apt install termux-api
am start -n org.fdroid.fdroid/org.fdroid.fdroid.views.main.MainActivity -d https://f-droid.org/es/packages/com.termux.api/
# Termux boot
am start -n org.fdroid.fdroid/org.fdroid.fdroid.views.main.MainActivity -d https://f-droid.org/es/packages/com.termux.boot/
```

## SSH Connection Configuration

For easier connection to servers via SSH and SFTP, you can use `MiXplorer` to connect to the `zero` server.

From the local machine, execute the following commands:

```sh
# Get remote absolute HOME path
COPY_X_HOME=$(ssh -i ~/.ssh/copy_x_cgt cgt@copy-x 'printf $HOME')
# Make .ssh folder
ssh -i ~/.ssh/copy_x_cgt cgt@copy-x "mkdir -p $COPY_X_HOME/.ssh"
# Send server private key
scp -P 2222 -i ~/.ssh/copy_x_cgt -r ~/.ssh/zero_cgt cgt@copy-x:$COPY_X_HOME/.ssh
```

In `MiXplorer`, add a new storage with the following data:

> Replace the variable `$COPY_X_HOME` before data insertion

| Field                         | Value                        |
|-------------------------------|------------------------------|
| URI address                   | sftp://zero.lan:2222/        |
| Username                      | cgt                          |
| Display name                  | Zero                         |
| Advanced settings -> key_path | `$COPY_X_HOME`/.ssh/zero_cgt |
| Advanced settings -> remote   | /media/data/pc-backup        |

## AI Server (Optional)

Recommended front ends:

- [ollama android app](https://apt.izzysoft.de/fdroid/index/apk/com.freakurl.apps.ollama) (f-droid)
- [Chatbox](https://play.google.com/store/apps/details?id=xyz.chatboxapp.chatbox)

Configure the server with `http://localhost:11434` endpoint

### Install

```sh
# Install dependencies
apt install git make golang
# Clone ollama repository
git clone --depth 1 https://github.com/ollama/ollama
# Build project
cd ollama
go build .
# Run service
./ollama serve
# Pull and run example (run in another window)
./ollama run deepseek-r1:1.5b
# kill process
pkill ollama
```

Move binary and clean data:

```sh
# Move compiled binary
mv $HOME/ollama/ollama /data/data/com.termux/files/usr/bin/
# Clean data
rm -r $HOME/ollama
```

### Shortcut Configuration and Startup

For the `Termux:Widget` plugin, add the following configurations to create shortcuts with custom commands:

```sh
# Create folder
mkdir -p $HOME/.shortcuts/ && cd $HOME/.shortcuts/
# Create script to start ssh server
echo "termux-wake-lock && ollama serve" > ollama-start.sh
# Create script to stop ssh server
echo "pkill ollama" > ollama-stop.sh
# Add execution permissions to all scripts
chmod +x *.sh
```

## Music Server (Optional)

```sh
# Install VLC and dependencies
apt install vlc cava
# Open CAVA (Audio visualizer)
cava
# Open a playlist in random mode
cvlc -I ncurses playlist.m3u8 --random --recursive expand
# Open a remote stream
cvlc --play-and-exit http://stream


# Open a playlist in random mode (http interface)
# https://wiki.videolan.org/Documentation:Alternative_Interfaces/
cvlc -I http --http-host localhost:8080 playlist.m3u8 --random --recursive expand

cvlc -I http --http-host 127.0.0.1:8080 /storage/06B0-1311/Music/Others/Others.m3u8 --random --recursive expand

cvlc -I ncurses /storage/06B0-1311/Music/Others/Others.m3u8 --random --recursive expand
```

```sh
# Code based on https://stackoverflow.com/a/40009032
tmux new-session \; split-window -h \; split-window -v -p 66 \; split-window -v \; select-pane -t 0 \; split-window -v -p 66 \; split-window -v \;
```