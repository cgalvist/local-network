# Multipurpose Server

## Installation

The server installation has been carried out on a modest desktop computer. This machine has been installed with `Ubuntu Server 24.04`, and initially, an SSH server with a weak password has been configured. Security improvements and additional configurations will be described later.

During the initial system installation, the following parameters were configured:

- **Installation mode:** `Minimal installation`
- **Install system on LVM partition:** Yes
- **Your name:** cgt
- **Your servers name:** zero (in lowercase)
- **Pick a username:** cgt
- **Install OpenSSH:** Yes

## Configuration

### Partition Configuration (LVM)

Test the server SSH connection

```sh
# Connect to server via SSH
ssh -p 22 cgt@zero
```

Ensure that the system is using the ENTIRE hard disk space with the command `df -h`. If not, follow these steps:

References: [Digital Ocean](https://www.digitalocean.com/community/tutorials/how-to-use-lvm-to-manage-storage-devices-on-ubuntu-18-04) and [fabianlee.org](https://fabianlee.org/2016/07/26/ubuntu-extending-a-virtualized-disk-when-using-lvm/).

- Observe detailed information about volumes:

```sh
sudo pvdisplay -m
```

- Increase the size of the main volume to 100% available. In this case, we are using the volume group `/dev/ubuntu-vg/ubuntu-lv` and the volume `/dev/sda3`:

```sh
LVM_GROUP=/dev/ubuntu-vg/ubuntu-lv
LVM_VOLUME=/dev/sda3
# Increase volume size
sudo lvextend -l +100%FREE $LVM_GROUP $LVM_VOLUME
# Make the change recognized by the system
sudo resize2fs $LVM_GROUP
```

- Finally, confirm that the main partition is using 100% of the available space by running the command `df -h` again.

### System Updates

Before proceeding with all the steps, it is recommended to update the system with the following commands:

```sh
sudo apt update
sudo apt -y upgrade
```

Install additional packages:

```sh
sudo apt -y install vim ufw htop rsync
```

### Change Time Zone

To change the time zone to Bogotá, run the following command:

```sh
sudo cp /usr/share/zoneinfo/America/Bogota /etc/localtime
```

After running this, verify that the time and date are correctly configured by running the `date` command.

### Change Password

Change the password to a stronger one by executing the `passwd` command.

### Disable Password for sudo

To prevent the system from prompting for a password every time we use sudo, run the `sudo visudo` command and add the following line:

```sh
# Command to avoid using a password with the sudo command
cgt ALL=(ALL) NOPASSWD:ALL
```

### Use Certificate for SSH Connections

On the local machine, generate the key with the following:

```sh
ssh-keygen -b 4096 -t rsa -f ~/.ssh/zero_cgt
```

The above command will create two files in the `.ssh` folder.

Now, to add the key on the server, type the following on the local machine:

```sh
ssh-copy-id -i ~/.ssh/zero_cgt.pub cgt@zero.lan
# Change permissions for the public key
sudo chmod 600 ~/.ssh/zero_cgt.pub
```

Finally, test the connection to see if it's working:

```sh
ssh cgt@zero.lan -i .ssh/zero_cgt
```

### Security Improvements (SSH)

Add the following configuration on the server (uncomment if necessary):

```sh
sudo vim /etc/ssh/sshd_config
```

```sh
Port 2222
PermitRootLogin no
PubkeyAuthentication yes
PasswordAuthentication no
UsePAM no

# Custom banner
Banner /etc/ssh/banner
```

Create a custom banner. (Text created with [Text to ASCII Art Generator](http://patorjk.com/software/taag/#p=display&f=Slant&t=Zero) using the `Slant` font):

```sh
sudo vim /etc/ssh/banner
```

```
                        _____                 
                       /__  /  ___  _________ 
                         / /  / _ \/ ___/ __ \
                        / /__/  __/ /  / /_/ /
                       /____/\___/_/   \____/ 

####################################################################
#                         Welcome to Zero                          #
#            All connections are monitored and recorded            #
#     Disconnect IMMEDIATELY if you are not an authorized user!    #
####################################################################
```

After changing the configuration, save and restart the service:

```sh
# Restart service
sudo /etc/init.d/ssh restart
# Restart server
sudo reboot now
```

Restart the connection to the server with the new port and the certificate generated earlier:

```sh
ssh cgt@zero.lan -p 2222 -i ~/.ssh/zero_cgt
```

### Firewall

Steps taken from [digitalocean.com](https://www.digitalocean.com/community/tutorials/como-configurar-un-firewall-con-ufw-en-ubuntu-18-04-es).

Initially, we will disable IPv6. To do this, edit the file `/etc/default/ufw`:

```sh
sudo vim /etc/default/ufw
```

And change the value `IPV6=no`.

By default, the firewall on Ubuntu Server is disabled. To enable it without losing the SSH connection, run the following commands:

```sh
# Reset all previous rules
sudo ufw reset
# Set default configuration
sudo ufw default deny incoming
sudo ufw default allow outgoing
# Open SSH server port
sudo ufw allow 2222/tcp comment 'Allow SSH access'
# Open web server ports
sudo ufw allow 80,443/tcp comment 'Allow Web access'
# Open NFS server ports
sudo ufw allow 111 comment 'Allow NFS access'
sudo ufw allow 2049 comment 'Allow NFS access'
# Open ports for torrent downloads
sudo ufw allow 6881 comment 'Allow Torrent access'
# Enable the firewall
sudo ufw enable
```

With these steps, the firewall will be configured. To view the rules running on the firewall, execute the following command:

```sh
sudo ufw status numbered
```

### Data Disk Encryption

In this case, only the data partition of the server will be encrypted. For system partition data encryption, it needs to be done during the OS installation.

#### Initial Setup

> Instructions taken from the page [alcancelibre.org](http://www.alcancelibre.org/staticpages/index.php/ciframiento-particiones-luks)

For this case, we will use the `sdb` disk (replacing the variable `DISK_NAME`). Before performing the steps, it is advisable to identify the partition to be used by running the command `sudo lsblk -o NAME,FSTYPE,SIZE,MOUNTPOINT,LABEL`.

> With these steps, the disk will be formatted, and data previously stored on it will be lost. Make a backup before the process if necessary.

If necessary, unmount the partitions of the disk to be encrypted with the following command:

```sh
DISK_NAME=sdb
# View a list of partitions in the system
df -h
# Unmount a partition
sudo umount /dev/$DISK_NAME
```

Encrypt the disk with the following command. You will have to answer with `YES` in uppercase and write a password for the disk:

```sh
sudo cryptsetup --verbose --verify-passphrase luksFormat /dev/$DISK_NAME
```

Then initialize the disk with the name `data`:

```sh
sudo cryptsetup luksOpen /dev/$DISK_NAME data
```

The above will create a new device at the path `/dev/mapper/data`.

Verify that the disk is encrypted with the following command:

```sh
sudo cryptsetup luksDump /dev/$DISK_NAME
```

The above command will display LUKS data for the disk, indicating password data for the disk (For now, it only has one).

For the operating system to be able to use the disk, proceed to format it in Ext4:

```sh
sudo mkfs.ext4 /dev/mapper/data
```

After having the partition ready, it is necessary to configure it in the system to mount it manually. For this, run the following commands:

```sh
sudo mkdir /media/data
sudo mount /dev/mapper/data /media/data/
```

#### Final Adjustments

Execute the following commands to finish configuring the data hard drive:

```sh
cd /media/data
# Create a folder with PC backup
sudo mkdir pc-backup
# Change the owner of all files in the partition
sudo chown cgt:cgt -R .
```

#### Automatic Partition Mounting

> Steps followed from the page [golinuxcloud.com](https://www.golinuxcloud.com/mount-luks-encrypted-disk-partition-linux/)

To automatically mount the encrypted partition, we will add an additional password to the partition with the following command:

```sh
sudo cryptsetup luksAddKey /dev/$DISK_NAME
```

> You can review the disk password information with the `luksDump` command to see information about the new password.

Now, create a `lukskey` file in the root of the system. This file will be used to access the disk on boot:

```sh
sudo dd if=/dev/random bs=32 count=1 of=/root/lukskey
```

Check the contents of the file with the command `sudo xxd /root/lukskey`. Then add this file as a key with the following command:

```sh
sudo cryptsetup luksAddKey /dev/$DISK_NAME /root/lukskey
```

> You can review the disk password information with the `luksDump` command to see information about the new access mode.

Then modify the `/etc/fstab` file and add the following line at the end of the file:

```sh
sudo vim /etc/fstab
```

```sh
# Mount the "data" partition
/dev/mapper/data      /media/data                 ext4    defaults        0 0
```

Finally, modify the `crypttab` file for the system to use the file to decrypt the disk. Add the following line at the end of the file:

```sh
sudo vim /etc/crypttab
```

```sh
# REPLACE $DISK_NAME WITH THE NAME OF THE PARTITION!
data  /dev/$DISK_NAME       /root/lukskey
```

After restarting the server, the partition should be mounted without prompting for any password. You can verify that the `data` partition has been mounted correctly with the `df -h` command.

## Web Server Configuration

This web server will use Traefik as a reverse proxy and will use other additional services as Docker containers.

### Docker Installation

> Steps from [docker.com](https://docs.docker.com/engine/install/ubuntu/)

To install Docker, run the following commands:

```sh
# Add Docker's official GPG key:
sudo apt -y install ca-certificates curl
sudo install -m 0755 -d /etc/apt/keyrings
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc
# Add the repository to Apt sources:
echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu $(. /etc/os-release && echo "$UBUNTU_CODENAME") stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
# Install Docker engine
sudo apt update
sudo apt -y install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
# Create a folder for Docker files
mkdir /media/data/docker
```

After installing the required programs, we will perform the post-installation steps (taken from [docs.docker.com](https://docs.docker.com/engine/install/linux-postinstall/)).

### Docker containers configuration

Create a new network `local-net`:

```sh
docker network create local-net
```

On the local machine, from the repository path, execute the following commands to copy and configure required container files and folders:

```sh
# Copy default files
scp -P 2222 -i ~/.ssh/zero_cgt -r src/zero/docker cgt@zero.lan:/media/data
```

On the remote machine, execute the following commands:

```sh
cd /media/data/docker
```

#### Reverse proxy (Traefik)

[Traefik](https://doc.traefik.io/traefik/) is a reverse proxy and load balancer compatible with `Docker`.

Steps derived from [technotim.live](https://technotim.live/posts/traefik-3-docker-certificates/).

Create a self-signed certificate:

```sh
cd /media/data/docker/data/proxy/certs
# Create a certificate with the above configuration
openssl req -x509 -sha256 -nodes -days 3650 -newkey rsa:2048 -keyout cert.key -out cert.crt  -config cert-config.cnf
# Add permissions
chmod 644 cert.crt
chmod 600 cert.key
```

Optionally, you can export the certificate to the `PKCS12` format to install it in a program or web browser:

```sh
# Export the certificate. You will need to enter a password after running the command
openssl pkcs12 -export -out cert-zero.p12 -inkey cert.key -in cert.crt
```

> The created certificate will be valid for one year, so when it expires, you need to create a new one.

Create a password for basic security in some containers:

```sh
# Install necessary program
sudo apt -y install apache2-utils
# Create a password hash for the admin user
htpasswd -nb admin $SECURE_PASSWORD
# Delete previous installed program
sudo apt -y purge apache2-utils
```

Save the generated hash in `/media/data/docker/.env` file with name `TRAEFIK_BASIC_PASS`.

### Docker containers deploy

Deploy all containers with `Docker Compose`:

```sh
cd /media/data/docker
docker compose up -d
```

#### Web File Browser (FileBrowser)

[File Browser](https://github.com/filebrowser/filebrowser) is a web-based file explorer that allows you to view, edit, and execute commands on server backup files.

To access the page, go to [zero.lan/files](https://zero.lan/files/) and log in with the following credentials:

- **User:** admin
- **Password:** admin

#### Configuration

1. After logging in, remember to change the password for the `admin` user to a stronger one.
   
2. In the main menu, select `Settings`, then the `Profile settings` tab, and choose `Use single clicks to open files and directories`. To save the changes, click the `UPDATE` button.

3. To change the appearance, go to the `Global Settings` tab, select the dark theme in the `Global Settings` section, and set the instance name as `zero`. Save the changes by clicking the `UPDATE` button.

#### Git Server (Gogs)

As a Git server, we'll use `Gogs`, a lightweight and resource-efficient Git server.

#### Configuration

1. Once the container is running, go to [git.zero.lan](https://git.zero.lan). You'll see a form to install Gogs for the first time. Modify the following details:

**Database Configuration**

| Field                | Value  |
|----------------------|--------|
| Database Type        | SQLite |

**General Configuration**

| Field               | Value                 |
|---------------------|-----------------------|
| Application Name    | Zero                  |
| SSH Port            | (Leave it blank)      |
| Application URL     | https://git.zero.lan/ |

**Optional Configuration**

- Server and Other Services Configuration
  - [x] Enable Offline Mode
  - [x] Disable Gravatar Service
  - [x] Disable Self-Registration
  - [x] Require Sign In to View Pages
- Administrator Account Configuration
  - Username: `admin_git`
  - Password: Create a strong password

Click `Install Gogs` to complete the installation.

#### Portainer Installation

Portainer is a graphical Docker management tool.

1. Go to [zero.lan/portainer](https://zero.lan/portainer/). You'll see a login window. Use the `admin_portainer` user and

 enter a strong password. Also, uncheck the box that allows sending anonymous data.

2. After creating the user, configure the site to manage the local Docker environment.

#### Downloader containers

For this download server, we'll use a container with a `JDownloader` web interface and another container for torrent downloads.

It's recommended to configure MyJdownloader for remote control from a smartphone.

#### Configuration

Access [zero.lan](https://zero.lan/downloader/) to use `JDownloader`.

Access [zero.lan](https://zero.lan/torrent/), and after logging in initially with the `admin` user and `adminadmin` password, change the password to a stronger one in `Options` -> `Web UI`.

### Password Manager

For the password manager, we'll use a container with `Firefox`.

1. Go to the [zero.lan/vault](https://zero.lan/vault/) page and log in with your `Firefox Sync` account. Then, set up a master password.

## NFS (optional)

With an NFS server, the transfer speed is much higher than with WebDav, making it optimal and convenient for streaming content.

One of the drawbacks is that its installation and configuration are a bit more complicated. Additionally, it's a protocol that hasn't been fully implemented on platforms other than Linux.

### Server

On the server, install the NFS server with the following command:

```sh
sudo apt install -y nfs-kernel-server
```

Open the NFS server configuration file and add the following line at the end to access the multimedia directory in read-only mode:

```sh
sudo vim /etc/exports
```

```sh
/media/data/pc-backup/Information/Multimedia/Videos 10.0.0.0/24(ro,sync,no_subtree_check,no_root_squash,fsid=0)
```

After changing the configuration, validate the data with the following command:

```sh
sudo exportfs -arv
```

Finally, restart the following services to enable the server:

```sh
sudo service nfs-kernel-server start
sudo service nfs-mountd restart
```

Test the service with the following command:

```sh
showmount -e localhost
```

### Client

On the remote machine (client), install the following package to mount the remote partition:

```sh
sudo apt install -y nfs-common
```

To mount the volume, execute the following command:

```sh
sudo mkdir -p /media/nfs/media_center
sudo mount -t nfs -o v4 zero:/ /media/nfs/media_center/ -v
```

To unmount the volume, use this command:

```sh
sudo umount -f -l /media/nfs/media_center
```

### Mount NFS Volume at Startup

To automate the mounting of the remote volume, open the `fstab` file and add the following content at the end:

```sh
sudo vim /etc/fstab
```

```sh
# Mount NFS partitions (zero)
zero:/    /media/nfs/media_center/   nfs auto,noatime,nolock,bg,nfsvers=4,intr,tcp,actimeo=1800 0 0
```

Run the following commands to enable the changes:

```sh
# Unmount the previously mounted partition (optional)
sudo umount -f -l /media/nfs/media_center
# Restart services
sudo systemctl daemon-reload
sudo systemctl restart remote-fs.target
```

With these steps, you should be able to remotely play multimedia content using programs like VLC or Kodi.

## Bot

Look at the [docs](../../../scripts/bots/zero/README.md)

## Useful Commands and Scripts

### Remove text from file names

For this, the `rename` package is required:

```sh
rename 's/TEXT_TO_REMOVE//' ./*TEXT_TO_REMOVE*
```

### Remove MKV video metadata

Script taken from [forums.plex.tv](https://forums.plex.tv/t/how-to-remove-tag-spam-and-set-language-in-several-mkv-files-at-once/235537/2).

The script removes possible junk from MKV format videos. The command currently only removes metadata from the title of the videos in a folder. For this, the `mkvtoolnix` package is required. You can find the `delete_metadata_mkv.sh` script in the server's script folder (in `scripts/others/zero/delete_metadata/`).

### Convert videos without recoding

Example of converting videos from `flv` to `mp4` without recoding:

```sh
for i in *.flv; do ffmpeg -i "$i" -codec copy "${i%.*}.mp4"; done
```

### Backup and restore LUKS header

[NixCraft](https://www.cyberciti.biz/security/how-to-backup-and-restore-luks-header-on-linux/)