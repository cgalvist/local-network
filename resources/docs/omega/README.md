# Media center

## Instalation

The system installation has been carried out on a modest desktop computer. This machine has been installed with `Kubuntu 24.04`, and initially, an SSH server with a weak password has been configured. Security improvements and additional configurations will be described later.

During the initial system installation, the following parameters were configured:

- **Installation mode:** `Minimal installation`
- **Name:** cgt
- **User:** cgt
- **Hostname:** omega (in lowercase)
- **Log in automatically without asking for the password:** `True`

After the installation, execute the following commands:

```sh
# Update software repositories
sudo apt update
# Install ssh server
sudo apt -y install openssh-server
# Enable ssh server service
sudo systemctl enable ssh
```

## Configuration

### Disable Password for sudo

Test the media center SSH connection

```sh
# Connect to media center via SSH
ssh cgt@omega
```

To prevent the system from prompting for a password every time we use sudo, run the `sudo visudo` command and add the following line:

```sh
# Command to avoid using a password with the sudo command
cgt ALL=(ALL) NOPASSWD:ALL
```

### Use Certificate for SSH Connections

On the local machine, generate the key with the following:

```sh
ssh-keygen -b 4096 -t rsa -f ~/.ssh/omega_cgt
```

The above command will create two files in the `.ssh` folder.

Now, to add the key on the server, type the following on the local machine:

```sh
ssh-copy-id -i ~/.ssh/omega_cgt.pub cgt@omega
# Change permissions for the public key
sudo chmod 600 ~/.ssh/omega_cgt.pub
```

Finally, test the connection to see if it's working:

```sh
ssh cgt@omega -i .ssh/omega_cgt
```

### Security Improvements (SSH)

Add the following configuration on the server (uncomment if necessary):

```sh
sudo apt -y install vim
sudo vim /etc/ssh/sshd_config
```

```sh
Port 2222
PermitRootLogin no
PubkeyAuthentication yes
PasswordAuthentication no
UsePAM no

# Custom banner
Banner /etc/ssh/omega_banner
```

Create a custom banner. (Text created with [Text to ASCII Art Generator](http://patorjk.com/software/taag/#p=display&f=Slant&t=Omega) using the `Slant` font):

```sh
sudo vim /etc/ssh/omega_banner
```

```
                   ____
                  / __ \____ ___  ___  ____ _____ _
                 / / / / __ `__ \/ _ \/ __ `/ __ `/
                / /_/ / / / / / /  __/ /_/ / /_/ /
                \____/_/ /_/ /_/\___/\__, /\__,_/
                                    /____/

####################################################################
#                         Welcome to Omega                         #
#            All connections are monitored and recorded            #
#     Disconnect IMMEDIATELY if you are not an authorized user!    #
####################################################################
```

After changing the configuration, save and restart the service:

```sh
# Restart ssh service
sudo /etc/init.d/ssh restart
```

Restart the server connection with the new port and the certificate generated earlier:

```sh
ssh cgt@omega -p 2222 -i ~/.ssh/omega_cgt
```

### Firewall

Initially, we will disable IPv6. To do this, edit the file `/etc/default/ufw`:

```sh
sudo vim /etc/default/ufw
```

And change the value `IPV6=no`.

By default, the firewall on Ubuntu Server is disabled. To enable it without losing the SSH connection, run the following commands:

```sh
# Reset all previous rules
sudo ufw reset
# Set default configuration
sudo ufw default deny incoming
sudo ufw default allow outgoing
# Open SSH server ports
sudo ufw allow 2222/tcp comment 'Allow SSH access'
# Open KDE Connect ports
sudo ufw allow 1714:1764/tcp comment 'Allow "KDE Connect" access (TCP)'
sudo ufw allow 1714:1764/udp comment 'Allow "KDE Connect" access (UDP)'
# Enable the firewall
sudo ufw enable
```

With these steps, the firewall will be configured. To view the rules running on the firewall, execute the following command:

```sh
sudo ufw status numbered
```

### Software

#### Update installed software

```sh
sudo apt update
sudo apt -y upgrade
```

#### Install required software

```sh
sudo apt -y install htop snapd kdeconnect wmctrl
sudo snap install firefox chromium
```

#### Install kodi

Steps from [flatpak.org](https://flatpak.org/setup/Ubuntu)

Install flatpak

```sh
sudo apt install -y flatpak
sudo flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
```

Add environment variable in `~/.profile`

```sh
# Flatpak setup
export XDG_DATA_DIRS="/var/lib/flatpak/exports/share:/home/cgt/.local/share/flatpak/exports/share:$XDG_DATA_DIRS" 
```

Install kodi

```sh
sudo flatpak install flathub tv.kodi.Kodi
```

Run kodi from cli

```sh
flatpak run tv.kodi.Kodi
```

#### Install shortcuts and scripts

On the local machine, run the following commands:

```sh
# Make folder for custom shortcuts and scripts
ssh cgt@omega -p 2222 -i ~/.ssh/omega_cgt 'mkdir -p $HOME/.local/share/applications'
# Copy shortcuts and scripts
scp -P 2222 -i ~/.ssh/omega_cgt -r scripts/others/omega/* cgt@omega:/home/cgt/.local/share/applications/
# Add script permissions
ssh cgt@omega -p 2222 -i ~/.ssh/omega_cgt 'chmod +x $HOME/.local/share/applications/open_single_instance.sh'
```