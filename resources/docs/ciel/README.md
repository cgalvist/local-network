# Home Router

## Installation

> It is recommended that the router is disconnected from the internet before configuring or updating the firmware.

The firmware was installed on an old router. This device was installed with `OpenWRT 21.02.1`.

> `OpenWRT 21.02.2` had problems installing with this router

## Initial configuration

At the system start, the router will always start with a LAN network of 192.168.1.0/24 and the domain [openwrt.lan](http://openwrt.lan). It is necessary to connect from a PC via a LAN port to change the network to 192.168.2.0/24.

We connect to the router via SSH with the following command:

```sh
ssh root@192.168.1.1
```

### Change host name

We will change the OpenWRT host name to its alias (ciel):

```sh
uci set system.@system[0].hostname='ciel'
uci commit system
/etc/init.d/system reload
```

### Assign addresses

#### DHCP

We access the graphical interface on the following [page](http://192.168.1.1). For the moment we can log in with the user `root` and without a password.

To assign a static address to the server for the external network and to change the addresses of the internal network, we go to the section `Network` -> `Interfaces`. The following options will appear:

* LAN
* WAN
* WAN6

To change the IP of the external network, click the `Edit` button for the `WAN` interface and change the values to the following:

- Tab `General Settings`

| Entry          | Value           |
|----------------|-----------------|
| Protocol       | Static address  |
| IPv4 Address   | 192.168.0.200   |
| IPv4 Netmask   | 255.255.255.0   |
| IPv4 Gateway   | 192.168.0.1     |
| IPv4 Broadcast | 192.168.0.255   |

- `Advanced Settings` Tab (`Next DNS`)

| Entry                     | Value        |
|---------------------------|--------------|
| Use Custom DNS Servers (1)| 45.90.28.254 |
| Use Custom DNS Servers (2)| 45.90.30.254 |

Finally click the `Save` button.

Then to change the addresses of the internal network, click the `Edit` button of the `LAN` tab and enter the following values:

- Tab `General Settings`

| Entry          | Value           |
|----------------|-----------------|
| Protocol       | Static address  |
| IPv4 Address   | 192.168.2.1     |
| IPv4 Netmask   | 255.255.255.0   |
| IPv4 Gateway   | 192.168.2.1     |
| IPv4 Broadcast | 192.168.2.255   |

- Tab `DHCP Server` -> `General Setup`

| Entry    | Value |
|----------|-------|
| Start    | 100   |
| Limit    | 50    |

- Tab `DHCP Server` -> `IPv6 Settings`: All parameters disabled

Finally click the `Save` button and then the `Save & Apply` button. Then we restart the device and verify that we have access to the new address.

> If you do not find a connection with the ip [192.168.2.1](http://192.168.2.1) restart the network service of your operating system and try again

> After this configuration, you can already connect the device to the internet

### Security improvements

We connect to the router via SSH with the following command:

```sh
ssh root@192.168.2.1
```

#### Change password

We change the root password with the `passwd` command.

#### Use certificate for SSH connections

##### Root

On the local computer, we do the following to generate the key. The file will be named `ciel`:

```sh
ssh-keygen -b 4096 -t rsa -f ~/.ssh/ciel
```

The previous command will create two files in the `.ssh` folder.

We go to `System` -> `Administration` -> `SSH-Keys` and add the key saved in the `ciel.pub` file.

We change the permissions of the certificate:

```sh
chmod 600 ~/.ssh/ciel.pub
```

And we try to authenticate again with the following command. It should not ask for a password:

```sh
ssh root@192.168.2.1 -i ~/.ssh/ciel
```

**Change SSH port**

From the OpenWRT web interface, we go to `System` -> `Administration` -> `SSH Access` -> `Dropbear instance` and change the following parameters:

| Entry                           | Value |
|---------------------------------|-------|
| Port                            | 2222  |
| Password authentication         | False |
| Allow root logins with password | False |

> Before making these changes, verify that public key authentication is working.

After clicking `Save & Apply`, we connect using the new port:

```sh
ssh root@192.168.2.1 -p 2222 -i .ssh/ciel
```

**Add banner**

We create a personalized banner. (Text created with [Text to ASCII Art Generator](https://patorjk.com/software/taag/#p=display&f=Slant&t=Ciel%0A) with the `Slant` font):

```sh
vim /etc/config/dropbear.banner.txt
```

```
                          _______      __
                         / ____(_)__  / /
                        / /   / / _ \/ / 
                       / /___/ /  __/ /  
                       \____/_/\___/_/   
#############################################################
#                      Welcome to Ciel                      #
#         All connections are monitored and recorded        #
# Disconnect IMMEDIATELY if you are not an authorized user! #
#############################################################
```

Now we configure the SSH server to show the banner every time an attempt is made to connect. For this, we edit the configuration:

```sh
vim /etc/config/dropbear
```

And we add the following line:

```
option BannerFile '/etc/config/dropbear.banner.txt'
```

### Deactivate unnecessary services

Delete the wireless network interfaces in `Network` -> `Wireless`

## Advanced configuration

### Address reservation

To reserve addresses for the main computers, we go to `Network` -> `DHCP and DNS` -> `Static Leases` and add the following entries:

| Hostname  | MAC Address           | IPv4 Address  |
|-----------|-----------------------|---------------|
| sigma     | (device MAC address)  | 192.168.2.200 |

We save and apply the changes. It may be necessary to disconnect and reconnect to the network for the host to obtain the configured IP.

### Configure firewall

We go to `Network` -> `Firewall` -> `Traffic rules` and add the following rules:

| Name | Protocol | Source zone | External port | Destination zone | Destination Port |
|-|-|-|-|-|-|
| Allow DNS Server for WAN | TCP, UDP | wan |  | Device (input) | 53 |

### Advertising blocker

> After configuring the ad blocker, remember to change the IP address of the ISP's default DNS for the router to point to the device.

To install the ad blocker, it is necessary to do it by command line. For this, we log in by SSH and enter the following commands:

(Taken from [openwrt.org](https://openwrt.org/docs/guide-user/services/ad-blocking))

```sh
# Install packages
opkg update
opkg install adblock wget tcpdump-mini

# Provide web interface
opkg install luci-app-adblock

# Backup the blocklists
uci set adblock.global.adb_backupdir="/etc/adblock"

# Save and apply
uci commit adblock
/etc/init.d/adblock restart
```

We restart the router. Then in the web interface we will see a new section `Services` -> `Adblock`.

In this section we activate the following options:

* `Force local DNS`: `true`
* `Forced zones`: [ `lan`, `wan` ]
* `Force local DNS`: [ 53, 853, 5353 ]
* `DNS Report`: `true`

With the previous steps, the ad blocker should now be working. You can test it by opening the [doubleclick.net](https://doubleclick.net) page. If the browser indicates that the page could not be found, the installation was successful.

> It may be useful to activate other sources to block more websites (for example, for parental control). However, make sure that the sources are not too large, as they can slow down the device.

### Configurations backup

> Run a backup after executing the above steps and installing the [Telegram bot](../../../scripts/bots/ciel/README.md)

It is always important to back up your router configuration. To do this, go to `System` -> `Backup / Flash firmware` and perform a backup every time you make a major change.