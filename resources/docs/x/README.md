# Main Server

## Remote Desktop

### Windows (10)

For Windows, we will use the RDP service included in the operating system.

To activate the service, right-click on `This PC`, then go to `Properties` -> `Remote Access Configuration`.

Enable the option `Allow remote connections to this computer`.

Then click on `Select users` and choose the user with which we will perform the remote access.

With this information, we can connect via RDP using the hostname "mr_x" and the user previously selected.

### Linux (Kubuntu)

> For a LAN network, it is best to use protocols like RDP or VNC, but the Linux machine had quite a few connection issues.

For Linux, we will use the `AnyDesk` program, with which we can create a remote desktop server very easily.

Install the repository and the program with the following commands (Taken from: [deb.anydesk.com](http://deb.anydesk.com/howto.html)):

```sh
sudo su
wget -qO - https://keys.anydesk.com/repos/DEB-GPG-KEY | apt-key add -
echo "deb http://deb.anydesk.com/ all main" > /etc/apt/sources.list.d/anydesk-stable.list
apt update
apt install anydesk
```

After installation, open the program and go to the `Set password for unattended access` link. A new tab will open; click on `Unlock Security Settings` and enter the system password.

**Optional:** If silent access is desired, in the `Interactive Access` section, select `Allow always` and enable the `Unattended Access` section by setting a secure password.

## SSH Server (X)

An SSH server will be created on the main PC (X) in Linux (Kubuntu) to allow access from other devices on the local network.

### Installation and Configuration

Install the program to create an SSH server on your machine:

```sh
sudo apt update
sudo apt install -y openssh-server
```

Add the following configuration (uncomment if necessary):

```sh
sudo vim /etc/ssh/sshd_config
```

```sh
Port 2222
PermitRootLogin no
UsePAM no
```

After changing the configuration, save and restart the service:

```sh
sudo /etc/init.d/ssh restart
```

On the remote machine (different from the main PC (X)), run the following to generate a public key:

```sh
ssh-keygen -b 4096 -t rsa -f ~/.ssh/mr_x_cgt
ssh-copy-id -p 2222 -i ~/.ssh/mr_x_cgt.pub cgt@mr-x
sudo chmod 600 ~/.ssh/mr_x_cgt.pub
ssh -p 2222 cgt@mr-x -i ~/.ssh/mr_x_cgt
```

After successfully accessing the main PC, on the main PC (X), disable password authentication to allow logins only with a certificate:

```sh
sudo vim /etc/ssh/sshd_config
```

```sh
PasswordAuthentication no
PubkeyAuthentication yes
```

After changing the configuration, save and restart the service again:

```sh
sudo /etc/init.d/ssh restart
```

With these steps, you can now access the main PC via SSH with a public key.

## SSH Connections Setup

To easily connect to servers via SSH and SFTP, run the command `vim ~/.ssh/config` and save the following configurations:

> Devices with OpenWRT are not configured because the SFTP connection presents errors

```conf
Host zero
     HostName zero.lan
     User cgt
     Port 2222
     IdentityFile ~/.ssh/zero_cgt

Host copy-x
     HostName copy-x.lan
     User cgt
     Port 2222
     IdentityFile ~/.ssh/copy_x_cgt
```

After this, you can connect via SFTP in a file explorer with the path `sftp://<host-name>/` (without the `.lan` extension).

### Firewall

Initially, we will disable IPv6. To do this, edit the file `/etc/default/ufw`:

```sh
sudo vim /etc/default/ufw
```

And change the value `IPV6=no`.

By default, the firewall on Ubuntu is disabled. To enable it, run the following commands:

```sh
# Reset all previous rules
sudo ufw reset
# Set default configuration
sudo ufw default deny incoming
sudo ufw default allow outgoing
# Open SSH server ports
sudo ufw allow 2222/tcp comment 'Allow SSH access'
# Open KDE Connect ports
sudo ufw allow 1714:1764/tcp comment 'Allow "KDE Connect" access (TCP)'
sudo ufw allow 1714:1764/udp comment 'Allow "KDE Connect" access (UDP)'
# Enable the firewall
sudo ufw enable
```

With these steps, the firewall will be configured. To view the rules running on the firewall, execute the following command:

```sh
sudo ufw status numbered
```