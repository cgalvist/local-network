# Personal network

Repository with files and documentation for a personal network

## Description

In this implementation of a home network, an `ASUS RT-N16` router with `OpenWRT` firmware will be used as the main server (router, firewall, DHCP server, SSH server, Telegram bot, etc.).

## Network diagram

![alt text](resources/img/network_diagram.png "Network diagram")

To improve security, we will use aliases for each of the main servers. For the internal network, the following will exist, inspired by characters from `Megaman X`:

* **Ciel:** Home router.
* **Sigma:** Private router.
* **Alia:** Wireless access point.
* **X:** Main PC.
* **Zero:** General server with Docker containers.
* **Vile:** Work PC.
* **Copy X:** Android smartphone.
* **Omega:** Media center.

### Repository settings

After cloning this repository, it is necessary to configure git to disable permission change scanning (for example, with chmod):

```sh
git config core.fileMode false
```

#### Other options

With the `@BotFather` bot from `Telegram`, we can change multiple options of our Bot, such as the name, profile image, etc. The use of these options can be found on the Internet.

## Home router

Read the [documentation](resources/docs/ciel/README.md)

## Private router

Read the [documentation](resources/docs/sigma/README.md)

## Multipurpose server (Zero)

Read the [documentation](resources/docs/zero/README.md)

## Main server (X)

Read the [documentation](resources/docs/x/README.md)

## Media center (X)

Read the [documentation](resources/docs/omega/README.md)

## Backups

Backups will be performed using scripts. You can consult their [documentation](scripts/backups/README.md).

## Useful commands and scripts

### View list of open ports assigned by process (IPv4)

```sh
sudo lsof -i4 -n -P | more
```

### Remote navigation with SCP from Dolphin (KDE)

Install the following package

```sh
sudo apt install kio-extras
```

Connect to the server

```sh
fish://cgt@10.0.0.1:2222/home/cgt/
```