#!/bin/bash

###############################
# Install shortcuts in system #
###############################

SYSTEM_APPS_PATH="$HOME/.local/share/applications"
SYSTEM_ICONS_PATH="$HOME/.local/share/pixmaps"

mkdir -p $SYSTEM_APPS_PATH
mkdir -p $SYSTEM_ICONS_PATH
find ./ssh/ -name '*.desktop' -exec cp -rf '{}' $SYSTEM_APPS_PATH ';'
find ../../resources/img/icons/ -name '*.png' -exec cp -rf '{}' $SYSTEM_ICONS_PATH ';'

chmod 644 $SYSTEM_APPS_PATH/ln-*.desktop
chmod 644 $SYSTEM_ICONS_PATH/ln-*

exit 0