#!/bin/bash

################################
# Open single instance process #
################################

if [ -z $1 ] ; then
  echo "Usage: open_single_instance.sh [process name]" && exit 1;
fi

PROCESS_NAME=$1

# Patch for chromium browser
if [[ "$PROCESS_NAME" == "chromium" ]] || [[ "$PROCESS_NAME" == "chromium-browser" ]]
    then
        pgrep chrome > /dev/null
    else
        pgrep $PROCESS_NAME > /dev/null
fi

if [ $? -eq 0 ]
    then
        wmctrl -a $PROCESS_NAME
    else
        # Patch for kodi (with flatpak)
        if [[ "$PROCESS_NAME" == "kodi" ]]
            then
                flatpak run tv.kodi.Kodi &
            else
                $PROCESS_NAME &
        fi        
fi

exit 0