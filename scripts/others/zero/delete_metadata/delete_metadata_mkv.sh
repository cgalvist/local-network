#!/bin/sh

###################################
# Delete metadata from MKV videos #
###################################

for f in *.mkv
do
 echo -n $f  ' '  
 mkvpropedit --delete title "${f}"
done

exit 0 
