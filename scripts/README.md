# Scripts

Folder with scripts used in the network.

* **backups:** Scripts to synchronize information between different devices.
* **bots_telegram:** Telegram bots scripts.
* **others:** Miscellaneous scripts.
* **shortcuts:** Script to install shortcuts on the system.