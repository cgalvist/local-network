#!/bin/bash

#######################################
# Synchronize music on Android device #
#######################################

SCRIPT_PATH="$( cd "$(dirname "$0")" ; pwd -P )"
MUSIC_DIR="Others/"
LOCAL_DIR="/media/cgt/Data/Information/Multimedia/Music/$MUSIC_DIR"
REMOTE_HOST=copy-x.lan
REMOTE_USER=cgt
REMOTE_PORT=2222
REMOTE_SD_CARD_NAME=06B0-1311
REMOTE_DIR="/storage/$REMOTE_SD_CARD_NAME/Music/$MUSIC_DIR"
KEY_PATH="~/.ssh/copy_x_cgt"

# Import colors
. "$SCRIPT_PATH/colors.sh"

# Make playlist in folder
cd "$LOCAL_DIR"
echo
"$SCRIPT_PATH/playlist_gen.py" "$LOCAL_DIR"
echo

echo -e "${RED}This script will update music on a remote device with data from this PC.${NC}"

echo -e "\n${YELLOW}The changes to be made are:${NC}\n"

# Show changes preview
rsync -rvl --protect-args --size-only --delete --dry-run -e "ssh -p $REMOTE_PORT -i $KEY_PATH" "$LOCAL_DIR" "$REMOTE_USER@$REMOTE_HOST:$REMOTE_DIR"

read -p "Are you sure you do it?(y/N) " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
  # Synchronize with device
  echo -e "\n${YELLOW}Sync files:${NC}\n"
  rsync -rvl --protect-args --size-only --delete -e "ssh -p $REMOTE_PORT -i $KEY_PATH" "$LOCAL_DIR" "$REMOTE_USER@$REMOTE_HOST:$REMOTE_DIR"
fi

# Show process result
if [ $? -eq 0 ]
    then
        echo -e "Synchronization finished: ${GREEN}OK${NC}"
    else
        echo -e "Synchronization finished: ${RED}ERROR${NC}"
        exit 1
fi

exit 0
