#!/bin/bash

############################################
# Synchronize files from laptop to main PC #
############################################

SCRIPT_PATH="$( cd "$(dirname "$0")" ; pwd -P )"
LOCAL_DIR=$HOME/Data/
REMOTE_HOST=mr-x
REMOTE_USER=cgt
REMOTE_PORT=2222
REMOTE_DIR=/media/cgt/Data/
IGNORE_FILE=ignore_data_laptop_lite.txt
KEY_PATH="~/.ssh/mr_x_cgt"

# Import colors
. "$SCRIPT_PATH/colors.sh"

echo -e "${RED}This script will update the data of the main PC with the data of this PC.${NC}"
echo -e "${RED}This synchronization is NOT bidirectional, so data that does not match at the destination can be deleted.${NC}"

echo -e "\n${YELLOW}The changes to be made are:${NC}\n"

# Show changes preview
rsync -rvlh --size-only --delete --dry-run --filter=':- .gitignore' --exclude-from=$IGNORE_FILE -e "ssh -p $REMOTE_PORT -i $KEY_PATH" $LOCAL_DIR $REMOTE_USER@$REMOTE_HOST:$REMOTE_DIR

read -p "Are you sure you do it?(y/N) " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
  # Overwrite data from local computer to main PC
  echo -e "\n${YELLOW}Sync files:${NC}\n"
  rsync -rvlh --size-only --progress --delete --filter=':- .gitignore' --exclude-from=$IGNORE_FILE -e "ssh -p $REMOTE_PORT -i $KEY_PATH" $LOCAL_DIR $REMOTE_USER@$REMOTE_HOST:$REMOTE_DIR
fi

# Show process result
if [ $? -eq 0 ]
    then
        echo -e "Synchronization finished: ${GREEN}OK${NC}"
    else
        echo -e "Synchronization finished: ${RED}ERROR${NC}"
        exit 1
fi

exit 0
