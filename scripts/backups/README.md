# Backup Scripts

> Assign execution permissions to the scripts if necessary.
> To execute the scripts, it is necessary to have `rsync` installed.

## Backup of Music to USB Drive

Script to create a backup of local music on a removable device.

Execution:

```sh
./scripts/backups/backup_music.sh
```

## Backup of Music to Android Device

> Wireless synchronization has encountered too many problems, whether via SSH or FTP. For now, synchronization can only be done using the microSD card as a conventional USB memory.

Script to create a backup of local music on an Android device and generate playlists.

1. The microSD card should be in the default Android format (Fat32).
2. Configure MiXplorer to create an FTP server, pointing to the music folder on the microSD card (`/storage/<PARTITION_ID>/Music/`).

Execution:

```sh
./scripts/backups/backup_android.sh
```

> Remember to configure VLC or the preferred player to search for music only in the folder `/storage/<PARTITION_ID>/Music`

## Backup of Data to Removable Hard Drive

Script to create a backup of the `Data` partition of the main computer on a removable hard drive.

**NOTE:** This script will overwrite all data on the removable hard drive and delete all files that do not match the source.

Execution:

```sh
./scripts/backups/backup_data.sh
```

## Backup of Music to External Device (Laptop)

Script to create a backup of the `Data` partition of the main computer on an external computer via SSH.

**NOTE:** This script will overwrite all data on the removable hard drive and delete all files that do not match the source.

**NOTE 2:** This script must be executed from the external computer that has SSH connection permissions.

> To avoid problems and reduce disk space consumption, this script will not synchronize some folders, such as the videos folder, the GIT folder, some game folders, and others.

Execution:

```sh
./scripts/backups/backup_laptop.sh
```

# Recommendations

## Encrypting Removable Hard Drive

For encrypting the hard drive, we will use LUKS.

In this case, the `sdb` disk will be used (replace the variable `DISK_NAME`). Before proceeding, it is advisable to identify the partition to be used by running the command `sudo lsblk -o NAME,FSTYPE,SIZE,MOUNTPOINT,LABEL`

**NOTE:** With these steps, the disk will be formatted, and the data previously stored on it will be lost. Make a backup before the process if necessary.

If necessary, unmount the partitions of the disk to be encrypted with the following command:

```sh
# View the list of partitions in the system
df -h
# Unmount a partition
sudo umount /dev/$PARTITION_NAME
```

Encrypt the disk with the following command. You will have to answer with `YES` in uppercase and enter a password for the disk:

```sh
sudo cryptsetup --verbose --verify-passphrase luksFormat /dev/$DISK_NAME
```

Then initialize the disk with the name `EXT_DISK`:

```sh
sudo cryptsetup luksOpen /dev/$DISK_NAME EXT_DISK
```

This will create a new device at the path `/dev/mapper/EXT_DISK`.

Check that the disk is encrypted with the following command:

```sh
sudo cryptsetup luksDump /dev/$DISK_NAME
```

The above command will show LUKS data for the disk, indicating password data for the disk (for now, it has only one).

To allow the operating system to use the disk, proceed to format it as ext4:

```sh
sudo mkfs.ext4 /dev/mapper/EXT_DISK
```

### Generate "keyfile"

To automatically mount the encrypted partition, add a file as a key `ext_disk.lukskey` with the following command:

```sh
mkdir ~/.luks
dd if=/dev/random bs=32 count=1 of=~/.luks/ext_disk.lukskey
```

Check the contents of the file with the command `sudo xxd ~/.luks/ext_disk.lukskey`. Then add this file as a key with the following command:

```sh
sudo cryptsetup luksAddKey /dev/$DISK_NAME ~/.luks/ext_disk.lukskey
```

> You can check the disk password information with the `luksDump` command to see information about the new password.

### Mount and Unmount the Partition

To avoid having to check the name of the disk each time it is connected, get the Universally Unique Identifier (UUID) of the disk to use it when mounting it on the computer.

```sh
sudo blkid | grep /dev/$DISK_NAME
```

In our case, we have obtained the UUID `0d0a0474-9264-4daf-8a17-0b89912496fc` (which replaces the variable `UUID`).

To mount the partition, you can run the following commands:

```sh
# Create a mount folder (only once)
sudo mkdir -p /media/cgt/EXT_DISK
# Initialize LUKS and mount partition
sudo cryptsetup --key-file=/home/cgt/.luks/ext_disk.lukskey luksOpen /dev/disk/by-uuid/$UUID EXT_DISK
sudo mount /dev/mapper/EXT_DISK /media/cgt/EXT_DISK/
# Change the owner of the disk (only once)
sudo chown -R cgt:cgt /media/cgt/EXT_DISK
```

To unmount the partition, run the following:

```sh
sudo umount /media/cgt/EXT_DISK
sudo umount /dev/mapper/EXT_DISK
sudo cryptsetup luksClose /dev/mapper/EXT_DISK
```
