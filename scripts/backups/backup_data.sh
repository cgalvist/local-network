#!/bin/bash

############################################
# Synchronize files on external hard drive #
############################################

SCRIPT_PATH="$( cd "$(dirname "$0")" ; pwd -P )"
REMOTE_HOST=zero
REMOTE_USER=cgt
REMOTE_PORT=2222
REMOTE_DIR=/media/data/pc-backup/
DEST_VOL=/dev/disk/by-uuid/0d0a0474-9264-4daf-8a17-0b89912496fc
DEST_VOL_NAME=EXT_DISK
DEST_NAME=/media/cgt/$DEST_VOL_NAME
DEST_DIR=$DEST_NAME/Backups/Zero/pc-backup/
IGNORE_FILE=ignore_data.txt
KEY_PATH="~/.ssh/zero_cgt"
LUKS_KEY_PATH="/home/cgt/.luks/ext_disk.lukskey"

# Import colors
. "$SCRIPT_PATH/colors.sh"

# Mount LUKS partition
echo -e "${YELLOW}Mount LUKS partition.${NC}\n"
sudo cryptsetup --key-file=$LUKS_KEY_PATH luksOpen $DEST_VOL $DEST_VOL_NAME
sudo mount /dev/mapper/$DEST_VOL_NAME $DEST_NAME

# Show process result
if [ $? == 0 ]
    then
        echo -e "LUKS mount finished: ${GREEN}OK${NC}"
    else
        echo -e "LUKS mount finished: ${RED}ERROR${NC}"
        exit 1
fi

echo -e "\n${RED}This script will backup the NAS server files to an external drive.${NC}\n"

echo -e "\n${YELLOW}The changes to be made are:${NC}\n"

# Show changes preview
rsync -rvlh --size-only --delete --dry-run --filter=':- .gitignore' --exclude-from=$IGNORE_FILE --rsync-path="sudo rsync" -e "ssh -p $REMOTE_PORT -i $KEY_PATH" $REMOTE_USER@$REMOTE_HOST:$REMOTE_DIR $DEST_DIR

read -p "Are you sure you do it?(y/N) " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
  # Sobrescribir datos del servidor al disco duro externo
  echo -e "\n${YELLOW}Sync files:${NC}\n"
  rsync -rvlh --size-only --progress --delete --filter=':- .gitignore' --exclude-from=$IGNORE_FILE --rsync-path="sudo rsync" -e "ssh -p $REMOTE_PORT -i $KEY_PATH" $REMOTE_USER@$REMOTE_HOST:$REMOTE_DIR $DEST_DIR

    # Show process result
    if [ $? == 0 ]
        then
            echo -e "Sync finished: ${GREEN}OK${NC}"
        else
            echo -e "Sync finished: ${RED}ERROR${NC}"
    fi
fi

# desmontar partición
echo -e "\n${YELLOW}Unmount LUKS partition.${NC}\n"
sudo umount $DEST_NAME
sudo umount /dev/mapper/$DEST_VOL_NAME
sudo cryptsetup luksClose /dev/mapper/$DEST_VOL_NAME

if [ $? -eq 0 ]
    then
        echo -e "LUKS umount finished: ${GREEN}OK${NC}"
    else
        echo -e "LUKS umount finished: ${RED}ERROR${NC}"
        exit 1
fi

exit 0
