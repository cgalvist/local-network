$RECYCLE.BIN/
Recovery/
System Volume Information/
lost+found/
found.*
.Trash-*
pagefile.sys
Thumbs.db
desktop.ini
Desktop.ini
*.tmp
._sync_*
.DS_Store
.ds_store
*.part
*.partial
*.filepart
*.crdownload
*.kate-swp
.fuse_hidden*
target/
node_modules/
Git/*
/Windows/*
/Docker/*
/Information/Multimedia/Videos/*
/Information/Programs/3D print/Models/**.gcode
