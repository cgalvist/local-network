#!/bin/bash

#######################################
# Synchronize music to external drive #
#######################################

SCRIPT_PATH="$( cd "$(dirname "$0")" ; pwd -P )"
MUSIC_DIR="Others/"
SOURCE_DIR="/media/cgt/Data/Information/Multimedia/Music/$MUSIC_DIR"
DEVICE_NAME=ANDROID
DESTINY_DIR="/media/cgt/$DEVICE_NAME/Music/$MUSIC_DIR"

# Import colors
. "$SCRIPT_PATH/colors.sh"

echo -e "${RED}This script will update the music on a USB stick with data from this PC.${NC}"

echo -e "\n${YELLOW}The changes to be made are:${NC}\n"

# Show changes preview
rsync -rvl --size-only --delete --dry-run $SOURCE_DIR $DESTINY_DIR

read -p "Are you sure you do it?(y/N) " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
  # Synchronize with device
  echo -e "\n${YELLOW}Sync files:${NC}\n"
  rsync -rvl --size-only --delete $SOURCE_DIR $DESTINY_DIR
fi

# Show process result
if [ $? -eq 0 ]
    then
        echo -e "Synchronization finished: ${GREEN}OK${NC}"
    else
        echo -e "Synchronization finished: ${RED}ERROR${NC}"
        exit 1
fi

exit 0
