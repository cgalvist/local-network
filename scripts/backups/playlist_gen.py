#!/usr/bin/python3
# -*- coding: utf-8 -*-
#coder :- Cesar Galvis

import os, sys, pathlib

"""
m3u playlist generator
"""

music_extensions = [".flac", ".m4a", ".mp3", ".ogg", ".wma"]


def valid_file(file_path):
    """
    Check if a filepath cointains a music file
    """
    ext = pathlib.Path(file_path).suffix
    return ext in music_extensions


def cast_file_data(index: int, file_path: str):
    """
    Cast filepath data for add in m3u playlist file
    """
    artist_name = os.path.dirname(file_path).split('/')[-1]
    ext = pathlib.Path(file_path).suffix
    track_title = pathlib.Path(file_path).name
    track_title = track_title.replace(ext, "")
    file_data = f"#EXTINF:{index},{artist_name} - {track_title}\n"
    file_data += f"{file_path}\n"
    return file_data


def get_relative_path(file_path):
    """
    Get relative path
    """
    file_name = pathlib.Path(file_path).name
    absolute_path = file_path.replace(file_name,"")
    base_path = os.path.basename(os.path.normpath(absolute_path))
    return f"./{base_path}/{file_name}"


def save_file(root_path, file_content, utf8_enabled):
    """
    Save text plain file
    """
    base_path = os.path.basename(os.path.normpath(root_path))
    file_name = f"{base_path}"

    if utf8_enabled:
        file_name += ".m3u8"
    else:
        file_name += ".m3u"

    text_file = open(f"{root_path}/{file_name}", "w")
    text_file.write(file_content)
    text_file.close()    


def main(argv):
    """
    m3u playlist generator
    """

    print("Playlist generator \U0001F3B5")

    # Get music path
    music_path = ""
    if len(argv) > 0:
        music_path = argv[0]
    else:
        print("Music path required \U00002757")
        return -1

    # Configure parameters
    utf8_enabled = True
    enable_absolute_paths = False
    playlist = ""

    if utf8_enabled:
        playlist = "#EXTM3U8\n"
    else:
        playlist = "#EXTM3U\n"

    # Read music file paths
    idx = 0
    for root, subdirs, files in os.walk(music_path):
        for file in files:
            if valid_file(file):
                idx += 1
                file_path = f"{root}/{file}"
                if not enable_absolute_paths:
                    file_path = get_relative_path(file_path)
                playlist += cast_file_data(idx, file_path)

    # Save playlist in file
    save_file(music_path, playlist, utf8_enabled)
    print("Process finished \U0001F600")


if __name__ == "__main__":
    main(sys.argv[1:])
