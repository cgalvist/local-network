# Telegram Bot (OpenWRT)

## Source code

Use code from this [repository](https://gitlab.com/cgalvist/personal-bots).

```sh
# Send binary to router
scp -O -i ~/.ssh/sigma -P 2222 target/mipsel-unknown-linux-musl/release/bot_router root@sigma.lan:/usr/bin/
# Send service script to router
scp -O -i ~/.ssh/sigma -P 2222 resources/scripts/bot_router root@sigma.lan:/etc/init.d/
```

After transferring the files, follow the instructions from the source code repository.