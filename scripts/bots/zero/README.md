# Telegram Bot (Ubuntu Server)

## Source code

Use code from this [repository](https://gitlab.com/cgalvist/personal-bots).

```sh
# Compile bot
cargo build --release -p bot_server
# Send binary
scp -P 2222 -i ~/.ssh/zero_cgt -r target/release/bot_server cgt@zero:/home/cgt
# Send service script
scp -P 2222 -i ~/.ssh/zero_cgt -r resources/ubuntu/systemd/telegram-bot.service cgt@zero:/home/cgt
```

Install bot with ssh

```sh
# Connect to SSH
ssh cgt@zero.lan -p 2222 -i ~/.ssh/zero_cgt
# Install bot binary
sudo mkdir -p /opt/TelegramBot/
sudo mv ~/bot_server /opt/TelegramBot/telegrambot
sudo chmod +x /opt/TelegramBot/telegrambot
# Install bot service
sudo mv ~/telegram-bot.service /lib/systemd/system/
sudo chmod 644 /lib/systemd/system/telegram-bot.service
```

Add secrets in the file `/lib/systemd/system/telegram-bot.service`, after the previous `[Service]` parameters:

```conf
[Service]
...
Environment="TELOXIDE_TOKEN=[ADD_SECRET_HERE]"
Environment="TELOXIDE_CLIENT_ID=[ADD_SECRET_HERE]"
```

Enable the service:

```sh
# Enabling service
sudo systemctl daemon-reload
sudo systemctl enable telegram-bot --now
# Check logs
journalctl -xeu telegram-bot
```