# Telegram Bots

## Privacy

By default, all Telegram bots are public and can be shared in groups.

We initially configure our bot so that it cannot be used in groups. To do this, we type the command `/setjoingroups` in the `@botfather` bot, select our bot, and then write the option `Disable`.

To prevent other users from using the bots individually, the scripts have a restriction that prevents users with an different identifier from the owner's from executing any function.